# RV32I RISC-V emulator


A simple RISC-V RV32I emulator written in Go



<img src="https://gitlab.com/behkar/rv32i/-/raw/master/logo.png"  width="350" height="400">


Note :

**This only executes some basic RV32I instructions, but more instructions will be added in the future**

## Roadmap & Features
- [x] add
- [x] and
- [x] addi
- [x] sw
- [ ] lui
- [ ] auipc
- [x] jal
- [ ] slti
- [x] beq
- [x] bne
## Installation


Run and install:
```bash
git clone https://gitlab.com/behkar/rv32i.git && cd rv32i
go mod tidy
go run .
```
For install:
```bash
go install .
```




## Author

- [@behkar](https://www.gitlab.com/behkar)
