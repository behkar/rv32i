package emu
// RV32I emulator code


// Registers
var (
	x  [32]uint32
	pc uint32
)

var mem [4096]uint32 

// Executes loaded program
func Exec() {
   for {

		if pc >= uint32(len(mem)*4) {

			println("Program exited")
			return
		}

	//	insn := mem[pc/4]

		// Rest of loop...

	}
	// Sample program
	mem[0] = 0x00500513 // addi x10, x0, 5
	mem[1] = 0x00028067 // jalr x0, 0(x1)
	mem[2] = 0x00000013 // nop

	// Load program
	pc = 0

	// Execute
	for {
		insn := mem[pc/4]
		opcode := insn & 0x7f

		switch opcode {
		case 0x13: // addi
			// addi code
		case 0x67:
			// jalr code
		case 0x00: // nop
			// nop code
		default:
			// default handler
		}

		pc += 4
	}

}


