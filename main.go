package main

import (
  "fmt"

  "gitlab.com/behkar/rv32i/asm" 
)

func main() {
  	program := []uint32{
		0x00130313, // ADDI x1, x0, 10
		0x01530313, // ADDI x2, x1, 20
		0x20230333, // ADD x3, x1, x2
	}


  // Run addition example
  emulator:=asm.Emulator{}
  emulator.LoadProgram(program)
  emulator.Run()
	for i, value := range emulator.Registers {
		fmt.Printf("x%d: %d\n", i, value)
	}  
  // Run subtraction example
  // asm.Sub()
}
