package asm

import (
	"fmt"
	"os"
)

// RISC-V RV32I emulator
type Emulator struct {
	// RISC-V registers
	Registers [32]int32
	// Program counter
	pc uint32
	// Memory
	memory []uint32
}

// Load program into memory
func (e *Emulator) LoadProgram(program []uint32) {
	e.memory = make([]uint32, len(program))
	copy(e.memory, program)
}

// Execute one instruction
func (e *Emulator) ExecuteInstruction() {
	// Check if program counter is out of bounds
	if e.pc >= uint32(len(e.memory))*4 {
		fmt.Println("Program counter out of bounds")
		os.Exit(1)
	}

	// Fetch instruction from memory
	instruction := e.memory[e.pc/4]
	// Decode and execute instruction
	opcode := instruction & 0x7F
	switch opcode {
	case 0x33: // R-type (arithmetic) instruction
		funct3 := (instruction >> 12) & 0x7
		rs1 := (instruction >> 15) & 0x1F
		rs2 := (instruction >> 20) & 0x1F
		rd := (instruction >> 7) & 0x1F

		switch funct3 {
		case 0x0: // ADD
			e.Registers[rd] = e.Registers[rs1] + e.Registers[rs2]
		case 0x7: // AND
			e.Registers[rd] = e.Registers[rs1] & e.Registers[rs2]
		default:
			fmt.Printf("Unsupported R-type instruction: %08x\n", instruction)
			os.Exit(1)
		}

	case 0x13: // I-type (immediate) instruction
		funct3 := (instruction >> 12) & 0x7
		rs1 := (instruction >> 15) & 0x1F
		imm := int32((instruction >> 20) & 0xFFF)
		rd := (instruction >> 7) & 0x1F

		switch funct3 {
		case 0x0: // ADDI
			e.Registers[rd] = e.Registers[rs1] + imm
		default:
			fmt.Printf("Unsupported I-type instruction: %08x\n", instruction)
			os.Exit(1)
		}

	case 0x6F: // JAL
		rd := (instruction >> 7) & 0x1F
		imm := int32(((instruction >> 31) & 0x1) << 20) | int32(((instruction >> 21) & 0x3FF) << 1) | int32(((instruction >> 20) & 0x1) << 11) | int32(((instruction >> 12) & 0xFF) << 12)
		e.Registers[rd] = int32(e.pc) + 4
		e.pc += uint32(imm)

	case 0x23: // S-type (store) instruction
		funct3 := (instruction >> 12) & 0x7
		rs1 := (instruction >> 15) & 0x1F
		rs2 := (instruction >> 20) & 0x1F
		imm := int32(((instruction >> 31) & 0x1) << 11) | int32(((instruction >> 25) & 0x3F) << 5) | int32(((instruction >> 7) & 0x1F) << 0)
		addr := e.Registers[rs1] + imm
		data := e.Registers[rs2]

		switch funct3 {
		case 0x2: // SW
			e.memory[addr/4] = uint32(data)
		default:
			fmt.Printf("Unsupported S-type instruction: %08x\n", instruction)
			os.Exit(1)
		}

	case 0x63: // B-type (branch) instruction
		funct3 := (instruction >> 12) & 0x7
		rs1 := (instruction >> 15) & 0x1F
		rs2 := (instruction >> 20) & 0x1F
		imm := int32(((instruction >> 31) & 0x1) << 12) | int32(((instruction >> 7) & 0x1) << 11) | int32(((instruction >> 25) & 0x3F) << 5) | int32(((instruction >> 8) & 0xF) << 1)
		branch := false

		switch funct3 {
		case 0x0: // BEQ
			branch = e.Registers[rs1] == e.Registers[rs2]
		case 0x1: // BNE
			branch = e.Registers[rs1] != e.Registers[rs2]
		default:
			fmt.Printf("Unsupported B-type instruction: %08x\n", instruction)
			os.Exit(1)
		}

		if branch {
			e.pc += uint32(imm)
			return
		}

	default:
		fmt.Printf("Unsupported instruction: %08x\n", instruction)
		os.Exit(1)
	}

	// Increment program counter
	e.pc += 4
}

// Run the emulator
func (e *Emulator) Run() {
	for e.pc < uint32(len(e.memory))*4 {
		e.ExecuteInstruction()
	}
}
